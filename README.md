# Пользовательская документация Takwot

Создано с помощью [VuePress](https://vuepress.vuejs.org/)
Вы можете редактировать .md-файлы с описанием прямо внутри репозитория.
Либо вы можете запустить локальный сервер разработки и работать на своей машине.

## Перед началом работы
Для запуска сервера разработки вам потребуется Node.js. Мы рекомендуем устанавливать Node с помощью [nvm](https://github.com/nvm-sh/nvm) – _node version manager_, с помощью него проще контролировать установленные версии. Но вы можете скачать репозиторий напрямую с [сайта](https://nodejs.org/en/download/) и установить его.
Также для контролирования версий зависимостей проекта мы используем [yarn](https://classic.yarnpkg.com/lang/en/). Вы можете воспользоваться встроенным в node.js менеджером пакетов npm, но точность версий зависимостей в таком случае не гарантирована.

## Установка зависимостей
```sh
yarn
```
или
```sh
npm install
```

## Запуск сервера разработки
```sh
yarn dev
```

или 

```sh
npm run dev
```

## Сборка статических файлов
```sh
yarn build
```

или 

```
npm run build
```

Путь к сборке `./src/.vuepress/dist`
