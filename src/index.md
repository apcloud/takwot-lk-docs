---
home: true
heroImage: /takwot-icon.png
tagline: Инструменты оптовой торговли и оказания услуг нового поколения
actions:
  - text: Быстрый старт →
    link: /guide/tochki-quick-start
    type: primary
  - text: Подробнее
    link: /guide/
    type: secondary

pageClass: home-page

features:
- title: 💪 Мощные
  # details: Feature 1 Description
- title: ⚡️ Быстрые
  # details: Feature 2 Description
- title: 🤳 Мобильные
  # details: Feature 3 Description
- title: 🛠️ Функциональные
  # details: Богатые возможности
- title: 💡 Удобные
  # details: Feature 5 Description
- title: 💅 Красивые
  # details: Feature 6 Description
footer: Сделано в «Агент Плюс» с ❤️
---
