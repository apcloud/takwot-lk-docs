// const { description } = require('../../package')
require('dotenv')
import slugify from 'slugify'
import type { UserConfig } from 'vuepress'

slugify.extend({ '.': '-' })

const config: UserConfig = {
  // title: 'Takwot',
  // description: description,
  head: [
    ['meta', { name: 'theme-color', content: '#14B8A6' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    [
      'meta',
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
    ],
    [
      'link',
      {
        rel: 'icon',
        href: process.env.IS_STAGING ? '/favicon-staging.ico' : '/favicon.ico',
      },
    ],
    [
      'link',
      {
        rel: 'icon',
        href: process.env.IS_STAGING ? '/favicon-staging.svg' : '/favicon.svg',
      },
    ],
  ],

  bundler: '@vuepress/bundler-vite',
  bundlerConfig: {
    viteOptions: {},
  },

  locales: {
    '/': {
      lang: 'ru',
      title: 'Takwot',
      description: 'Справка Takwot',
    },
    '/uk/': {
      lang: 'uk',
      title: 'Takwot',
      description: 'Довідка Takwot',
    },
  },

  themeConfig: {
    logo: '/logo.png',
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    contributors: false,
    locales: {
      '/': {
        selectLanguageText: 'Переводы',
        selectLanguageName: 'Русский',
        selectLanguageAriaLabel: 'Переводы',

        serviceWorker: {
          updatePopup: {
            message: 'Доступно новое содержимое',
            buttonText: 'Перезагрузить',
          },
        },

        navbar: [
          // {
          //   text: 'О приложении',
          //   link: '/about/',
          // },
          {
            text: 'ЛК',
            link: 'https://pa.takwot.com/',
          },
          {
            text: 'Точки СВ',
            link: 'https://tochki.takwot.com/',
          },
          {
            text: 'Заказы ЛК',
            link: 'https://orders-operator.takwot.com/',
          },
          {
            text: 'Поддержка',
            link: '/guide/common-support.md',
          },
        ],

        sidebar: {
          '/guide/': [
            {
              text: 'Личный кабинет',
              collapsable: false,
              children: [
                '/guide/pa-quick-start.md',
                '/guide/pa-settings.md',
                '/guide/pa-users.md',
                '/guide/pa-balance.md',
                '/guide/lk-updates/ver.md',
              ],
            },
            {
              text: 'Веб-сервис «Точки СВ»',
              collapsable: false,
              children: [
                '/guide/tochki-quick-start.md',
                '/guide/tochki-departments.md',
                '/guide/tochki-staff.md',
                '/guide/tochki-map.md',
                '/guide/tochki-map-tools.md',
                '/guide/tochki-report-movement.md',
                '/guide/tochki-report-visits.md',
                '/guide/tochki-mobile-apps.md',
                '/guide/tochki-updates/ver.md',
              ],
            },
            {
              text: 'Мобильное приложение «Точки»',
              collapsable: false,
              children: [
                '/guide/tochki-android-quick-start.md',
                '/guide/tochki-android-update-version.md',
                {
                  text: 'Настройки энергосбережения',
                  collapsable: false,
                  children: [
                    '/guide/tochki-android-bkg-mode.md',
                    '/guide/tochki-android-bkg-mode-xiaomi.md',
                    '/guide/tochki-android-bkg-mode-huawei.md',
                    '/guide/tochki-android-bkg-mode-honor.md',
                    '/guide/tochki-android-bkg-mode-realme.md',
                    '/guide/tochki-android-bkg-mode-oppo.md',
                  ],
                },
                '/guide/tochki-and-mt20.md',
                '/guide/ver-mobile/ver.md',
              ],
            },
            {
              text: 'Веб-сервис «Заказы»',
              collapsable: false,
              children: [
                '/guide/orders-quick-start.md',
                '/guide/company-chats.md',
                '/guide/company-catalog.md',
                '/guide/company-vendors.md',
                '/guide/company-orders.md',
              ],
            },
            {
              text: 'Веб-приложение «Заказы»',
              collapsable: false,
              children: [
                '/guide/vendor-quick-start.md',
                '/guide/vendor-orders.md',
                '/guide/vendor-company.md',
                '/guide/vendor-cart.md',
                '/guide/vendor-settings.md',
              ],
            },
            {
              text: 'Общая информация',
              collapsable: false,
              children: [
                '/guide/common-faq.md',
                '/guide/common-ver.md',
                '/guide/common-support.md',
              ],
            },
          ],
          // '/lk-updates/': [
          //   {
          //     title: 'Обновления ЛК',
          //     collapsable: false,
          //     children: ['1.0.0-rc.18'],
          //   },
          // ],
        },
      },
      '/uk/': {
        selectLanguageText: 'Переклади',
        selectLanguageName: 'Український',
        selectLanguageAriaLabel: 'Переклади',

        serviceWorker: {
          updatePopup: {
            message: 'Доступно новое содержимое',
            buttonText: 'Перезагрузить',
          },
        },

        navbar: [
          {
            text: 'Оператор',
            link: 'https://pa.takwot.com/',
          },
          {
            text: 'Заказы ЛК',
            link: 'https://orders-operator.takwot.com/',
          },
          {
            text: 'Сервис Точки',
            link: 'https://tochki.takwot.com/',
          },
        ],
        sidebar: {
          '/lk-updates/': [
            {
              title: 'Обновления ЛК',
              collapsable: false,
              children: [''],
            },
          ],
        },
      },
    },
  },

  // plugins: ['@vuepress/plugin-back-to-top', '@vuepress/plugin-medium-zoom'],
  plugins: [['@vuepress/plugin-search', {}]],

  // devServer: {
  //   headers: {
  //     'Access-Control-Allow-Origin': '*',
  //     'Access-Control-Allow-Methods': 'GET, OPTIONS',
  //     'Access-Control-Allow-Headers': 'X-Requested-With, content-type',
  //   },
  // },

  markdown: {
    toc: { slugify },
    anchor: { slugify },
    extractHeaders: { slugify },
  },
}

export default config
