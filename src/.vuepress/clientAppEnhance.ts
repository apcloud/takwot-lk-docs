import { defineClientAppEnhance } from '@vuepress/client'

import UpdatesLayout from './components/UpdatesLayout.vue'

export default defineClientAppEnhance(({ app }) => {
  app.component('UpdatesLayout', UpdatesLayout)
})
