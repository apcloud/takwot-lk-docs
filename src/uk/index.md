---
home: true
heroImage: /takwot-icon.png
tagline: Інструменти оптової торгівлі нового покоління
actionText: Швидкий старт →
actionLink: /uk/guide/all-intro
pageClass: home-page
secondaryText: Детальніше
secondaryLink: /uk/guide/
features:
- title: 💪 Відчуй силу
  # details: Feature 1 Description
- title: ⚡️ Блискавична швидкість
  # details: Feature 2 Description
- title: 🤳 Мобільність
  # details: Feature 3 Description
- title: 🛠️ Багаті можливості
  # details: Feature 4 Description
- title: 💡 Зручність управління
  # details: Feature 5 Description
- title: 💅 Гарний інтерфейс
  # details: Feature 6 Description
footer: Зроблено людьми с ❤️
---
