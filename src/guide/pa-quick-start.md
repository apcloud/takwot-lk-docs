# Быстрый старт

::: warning Внимание
Для выполнения настроек нужна учетная запись администратора.
:::

## Введение
«Личный кабинет» предназначен для настройки и администрирования веб-сервисов **Takwot (Такво́т)**.

На текущий момент сервис Takwot поддерживает следующие возможности:
- [Таквот.Точки](/guide/tochki-quick-start.html) — контроль передвижений выездных сотрудников и контроль выполнения задач выездных сотрудников. 
- [Таквот.Заказы](/guide/vendor-quick-start.html) — прием заказов выездными сотрудниками на поставку товаров от поставщика в торговые точки (в т.ч. внештатными сотрудниками поставщика).

## Вход в личный кабинет
Для входа в «Личный кабинет» перейдите по ссылке [https://pa.takwot.com](https://pa.takwot.com) и введите номер телефона на который вам приходило СМС-приглашение стать администратором сервисов **Таквот**.

## Настройка компании
Перед началом работы нужно заполнить профиль компании и заполнить график работы компании.
В качестве компании выступает в данном случае юридическое лицо, использующее веб-сервисы **Takwot**.

Перейдите в **Профиль компании**, для этого: 
1. Кликните по полю **ЛК** в левом верхнем углу страницы и выберите пункт **Компании**.
2. Если компаний несколько, выберите нужную и прокрутите страницу вниз до секции **Расписание**.
3. Заполните основные поля: название компании, описание, при необходимости - укажите логотип компании.
4. Заполните расписание работы компании и обязательно укажите часовой пояс. Если рабочие часы в разные дни недели отличаются, то нажмите на кнопку **Настроить подробное расписание**.

::: warning Внимание
Расписание работы компании и часовой пояс используются как расписание «по умолчанию» для ведения GPS-трекинга у [выездных сотрудников](/guide/tochki-staff.html#Roli-sotrudnikov).
:::

Измененные настройки сохраняются автоматически.

[Подробнее о настройках](/guide/pa-settings.html)

![Редактирование профиля компании](./images/company_profile.gif)

