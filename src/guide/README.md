# Введение

**Takwot (Такво́т)** - это облачный сервис для использования поставщиками в сферах продаж товаров или оказания услуг. 

На текущий момент сервис Takwot поддерживает следующие возможности:
- [Таквот.Точки](/guide/tochki-quick-start.html) - контроль передвижений выездных сотрудников и контроль выполнения задач выездных сотрудников. 
- [Таквот.Заказы](/guide/vendor-quick-start.html) - прием заказов выездными сотрудниками на поставку товаров от поставщика в торговые точки (в т.ч. внештатными сотрудниками поставщика).


::: tip Про удобство
Если у вас возникнут какие-либо пожелания или вам покажется что-то неудобным, непонятным. Или если вам что-то мешает. Или у вас есть идеи как что-то можно сделать лучше. Сообщите нам в [службу поддержки](/guide/common-support.html). Мы очень хотим чтобы работать с Takwot вам было легко и удобно.
:::

::: warning Про ошибки
При возникновении ошибок пожалуйста сообщите нам [службу поддержки](/guide/common-support.html). Мы сможем исправить ошибку только когда о ней узнаем.
:::
