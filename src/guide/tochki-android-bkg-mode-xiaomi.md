# Xiaomi

::: tip Примечание
Рекомендуется предварительно ознакомиться со статьей [Энергосбережение](/guide/tochki-android-bkg-mode.html).
:::

<h3>Настройки энергосбережения для приложения «Точки» на мобильных устройствах Xiaomi</h3>

1. Войти в настройки ОС Android, например, нажав на кнопку «Шестерёнка» в верхней выдвижной панели:

![Войти в настройки Android](./images/xiaomi_bkg_mode_1.jpg)

2. В поле поиска настройки введите слово «Батарея», выберите настройку «Батарея»:

![Выбрать настройку «Батарея»](./images/xiaomi_bkg_mode_2.jpg)

3. В окне «Батарея» прокрутите окно вниз и найдите в списке приложение «Точки»:

![Найти приложение «Точки»](./images/xiaomi_bkg_mode_3.jpg)

4. В открывшемся окне внизу нажмите на кнопку «Сведения»:

![Нажмите на кнопку «Сведения»](./images/xiaomi_bkg_mode_4.jpg)

5. В открывшемся окне «Сведения» убедитесь, что включен переключатель «Автозапуск», после нажмите пункт «Контроль активности»:

![Переключатель «Автозапуск» и «Контроль активности»](./images/xiaomi_bkg_mode_5.jpg)

6. В открывшемся окне выберите самый верхний пункт «Нет ограничений»:

![Переключатель «Автозапуск» и «Контроль активности»](./images/xiaomi_bkg_mode_6.jpg)


После выполненных шагов операционная система прекратит останавливать работу приложения в фоновом режиме.
