# Версии

Ниже приводятся ссылки на описания версий всех компонент веб-сервисов **Takwot (Такво́т)**.

* [Версии](/guide/lk-updates/ver.html) веб-сервиса [«Личный кабинет»](/guide/pa-quick-start.html#Vvedenie)
* [Версии](/guide/tochki-updates/ver.html) веб-сервиса [«Точки-СВ»](/guide/tochki-quick-start.html#Vvedenie)
* [Версии](/guide/ver-mobile/ver.html) мобильного приложения [«Точки»](/guide/tochki-android-quick-start.html#Vvedenie)

