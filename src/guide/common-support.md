# Служба поддержки

Наши контакты:
* :speech_balloon: [Skype](https://join.skype.com/invite/LjBuSxRyneeX)
* :envelope: <support@takwot.com>
* :phone: <a href="tel:8 800 333-63-21">8 800 333-63-21</a>, <a href="tel:+7 (495) 662-59-01">+7 (495) 662-59-01</a>
