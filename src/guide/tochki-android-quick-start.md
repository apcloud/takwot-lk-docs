# Быстрый старт


## Введение

### Мобильное приложение «Точки»
Мобильное приложение «Точки» – это нативное приложение для смартфонов и планшетов, работающих под управлением ОС Android. Приложение используется выездными сотрудниками для передачи в веб-сервис «Точки СВ» данных о своих передвижениях, а также данных о действиях в приложении [«Агент Плюс: Мобильная торговля»](https://agentplus.ru/mt/). _Но использование приложения «Агент Плюс: Мобильная торговля» необязательно._

![Мобильное приложение «Точки»](./images/tochki-mobile-app.webp)

### Роли пользователей
Приложение «Точки» могут использовать только пользователи с ролью [«Выездной сотрудник»](/guide/tochki-staff.html). При этом у сотрудника может быть одновременно несколько ролей.

## Установка и запуск приложения

### Установка приложения
Приложение «Точки» устанавливается на мобильное устройство при скачивании по ссылке [http://takwot.com/files/upd/tochki.apk](http://takwot.com/files/upd/tochki.apk).

### Запуск приложения
При первом запуске приложение попросит пользователя ввести номер мобильного телефона для авторизации пользователя. После ввода номера телефона пользователь получит СМС с кодом подтверждения для входа в приложение.

::: warning Предупреждение
Предварительно администратор должен добавить пользователя в веб-сервис [«Точки СВ»](/guide/tochki-quick-start.html) с назначенной ролью [«Выездной сотрудник»](/guide/tochki-staff.html).
:::

::: details Переадресация SMS
Если по каким-либо причинам пользователь не получает SMS-сообщения, то отправьте письмо на адрес <support@takwot.com> с просьбой включить переадресацию SMS. В письме нужно указать номер телефона и адрес электронной почты для пеереадресации SMS-сообщений.<br>
После включения переадресации все SMS-сообщения от Веб-сервиса Takwot для указанного телефона будут пересылаться на закрепленный за номером телефона почтовый ящик.
:::

Приложение «Точки» не нужно регулярно запускать на мобильном устройстве - оно продолжит работать в фоновом режиме после его закрытия, или даже после перезапуска мобильного устройства. Главное, чтобы пользователь был авторизован в приложении по номеру мобильного телефона.<br>
Для авторизации пользователя **не обязательно**, чтобы приложение было установлено на смартфоне или планшете с подключенной SIM-картой, т.к. при авторизации SMS можно получать на отдельном смартфоне или даже отдельном кнопочном телефоне. Пользователь просто должен иметь возможность прочитать текст SMS-сообщения и ввести код из этого сообщения.

### Энергосбережение

::: danger Внимание!
Мобильные устройства некоторых производителей требуется дополнительно настроить для фоновой работы приложения «Точки».
:::

У мобильных устройств разных производителей есть специфичные настройки энергосбережения — эти настройки влияют на фоновую работу приложения «Точки». Если у вас используется мобильное устройство производителя из списка ниже, то обязательно включите на нём дополнительные [настройки энергосбережения](/guide/tochki-android-bkg-mode.html): 
* Honor
* Huwaei
* Oppo
* Realme
* Xiaomi

