---
layout: UpdatesLayout
---

## 0.18.0
<p class="c-release-date-wrap"><time class="c-release-date">10 февраля 2022</time></p>

### Возможность входа в систему с помощью номера телефона входящего вызова
![Вид панели авторизации](./images/incoming-call-phone-number-auth-20220210.webp)


## 0.17.0
<p class="c-release-date-wrap"><time class="c-release-date">23 декабря 2021</time></p>

Места простоя выездного сотрудника на карте выводятся в виде полупрозрачного круга с обводкой цвета трека

![Место простоя на карте](./images/agent-idle-point-20211229.jpg)

Вывод времени простоя в отчёте передвижений

![Время простоя в отчёте](./images/agent-idle-table-20211229.jpg)

Cвязь между маркерами вендора и местом задачи, которую выполнили удалённо, отображется в виде красной пунктирной линии.

![Cвязь между маркерами вендора и удаленно-выполненной задачи](./images/tasks-relation-link-20211229.jpg)

Команды мобильного устройства выездного сотрудника теперь доступны для администратора в диалоге «Данные выездного сотрудника»

![Расположения действия открытия диалога](./images/employee-details-20201229.jpg)

![Вид диалога с действиями для мобильного устройства](./images/employee-details-actions-20201229.jpg)

Если у задачи есть прикрепленные изображения, то они будут выводиться на карте во всплывающей карточке задачи и в списке задач.

![Изображения задачи](./images/task-images-20211229.jpg)

Вывод количества сотрудников на экране «Персонал»

![Количество сотрудников](./images/employees-count-20211229.jpg)


## 0.15.0
<p class="c-release-date-wrap"><time class="c-release-date">18 ноября 2021</time></p>

### Возможность удаления точек трека администратором

Часто координаты GPS могут быть неточными. А иногда случаются аномальные «вылеты» трека, которые портят данные о перемещениях. Мы работаем над тем чтобы автоматически убирать эти аномалии. Но пока мы открываем возможность для администраторов самим удалять аномальные точки трека.

![Включить инструменты редактирования трека](./images/point-delete-tool-activate-20211117.jpg)

Чтобы удалить точки трека нужно открыть инструменты редактирования, через выпадающее меню выездного сотрудника.
В открытых инструментах выбрать «Выбор точек». Выбрать аномальные точки.
Нажать в инструментах «Предпросмотр трека». Для сохранения изменений кликнуть «Сохранить изменения»

![Удаление точек трека](./images/point-delete-process-20211117.gif)

### Управление задачами выездных сотрудников

Теперь супервайзер или администратор может самостоятельно создавать, переназначать или редактировать задачи для выездных сотрудников.


### Новый вид значков маркеров карты
- ![Вид маркера ожидает выполнения](./images/task-marker-icon--await.png) Задача ожидает выполнения.
- ![Вид маркера выполнено частично](./images/task-marker-icon--half.7c9878e.png) Частично выполнено. Если у точки есть несколько задач. И часть задач была выполнена.
- ![Вид маркера вендор посещён](./images/task-marker-icon--shop.f4d4c19.png) Задача связанная с этой точкой была успешно выполнена.
- ![Вид маркера выполнено уделённо](./images/task-marker-icon--ok.3eebe7d.png) Удалённое выполнение. Дополнительный маркер выполнения задачи. Показывает где находился сотрудник когда была выполнена задача, если выполнение произошло на расстоянии от вендора.
- ![Вид маркера не выполнено](./images/task-marker-icon--warning.png) Не выполнено. Выполнение задачи просрочено на 24 часа. Или точка была посещена, но задача не была выполнена.
- ![Вид маркера начала трека](./images/start-marker-20211117.png) ![Вид маркера конца трека](./images/end-marker-20211117.png) Маркеры начала и конца трека.


## 0.11.0
<p class="c-release-date-wrap"><time class="c-release-date">8 сентября 2021</time></p>

### Обновлённый «Отчет о передвижениях»

Отчёт приобрел новый вид. При желании можно увидеть детализацию перемещений выездных сотрудников по каждому дню в выбранном периоде.

![Вид отчёта](./images/travel-report-update-20210908.jpg)

Параметры отчёта вынесены в диалог настроек: цена за 100 км, период отчёта, формат детализации в Excel.

![Вид диалога настроек](./images/travel-report-settings-20210908.jpg)

Если при формировании отчёта выставить галочку «Детализация по дням», то при скачивании отчёта в формате Excel будет доступны данные по каждому дню выбранного периода.

### Возможность копировать координаты в сплывающих диалогах

![При клике на маркере](./images/copy-coords-marker-20210908.jpg)

![При клике на карте](./images/copy-coords-point-20210908.jpg)

### Ссылка на свежее android-приложение

Администаторы могут копировать ссылку на свежее android-приложение «Точки» на экране «Мобильные приложения»

![Расположение на больших экранах](./images/copy-android-app-link-20210908.jpg)

В мобильной версии кнопка расположена в правом верхнем углу экрана слева от поиска

![Вид на мобильной вверсии](./images/copy-android-app-link-mobile-20210908.jpg)


## 0.10.0
<p class="c-release-date-wrap"><time class="c-release-date">1 сентября 2021</time></p>

### Экран «Описание релиза»

При появлении значимых изменений в приложении мы будем показывать вам это диалоговое окно. Его можно вызвать самостоятельно из выпадающего меню.

![Вызов диалога изменений](./images/relase-notes-link.jpg)


## 0.9.3
<p class="c-release-date-wrap"><time class="c-release-date">18 августа 2021</time></p>

### Действия выездных сотрудников
Справа от имени сотрудника в списке находятся его действия.
Кнопка со значком «три вертикальные точки» скрывает или показывает меню дополнительных действий. Кнопка со значком глаза позволяет скрыть или показать трек сотрудника.
Если кликнуть по кнопке со значком глаза с зажатой клавишей Alt то будут скрыты треки и события всех остальных сотрудников.

![Список действий выездного сотрудника](./images/agent-actions-list.jpg)

В дополнительном меню есть ещё четыре дейтвия:
- **Перейти к треку**. Переносит положение карты к треку сотрудника и если нужно дозагружает данные.
- **Данные выездного сотрудника**. Показывает диалог с дополнительными данными сотрудника. Пока там находится расписание его работы, если таковое установлено.
- **Скрыть остальных сотрудников**. Скрывает данные всех сотрудников, кроме текущего.
- **Инспекция трека**. Включает или выключает режим инспекции трека.


### Действия «скрыть всех» и «показать всех»
Скрыть всех — скрывает все треки и события выездных сотрудников. Показать всех — соответсвенно показывает все доступные треки и события.

### Инспектор трека
С помощью инспектора трека можно подробнее проанализировать в какое время и в какой точке трека был сотрудник. Передвижение сотрудника можно контролировать перетаскивая положения бегунка внизу.

![Режим инспектора трека](./images/track-inspector-view.jpg)

Риски на бегунке обозначают отрезки трека. При наведении на отрезок на бегунке можно увидеть соответствующий период времени в котором записаны координаты этого отрезка.

Инспектор трека можно включить в меню действий сотрудника.

![Действие «Инспекция трека»](./images/track-inspector-action.jpg)

### Принятие приглашения, при авторизации
При попытке авторизации пользователя, который ещё не принял приглашение система будет предлагать принять его. После успешного принятия пользователь будет авторизирован в системе.

### Вывод координат в попапе при клике по карте
![Попап с координатами](./images/lat-lng-popup.jpg)

### Экран «Мобильные приложения»
Расширили отчёт версий приложения дополнительными функциями и переименовали экран в Мобильные приложения.

В строке сотрудника теперь доступны команды для мобильного приложения

![Команды для мобильного приложения](./images/versions-report-app-commands.jpg)

По двойному клику на строке отчёта можно попасть в детализацию сотрудника

По клику на текущей версии можно скопировать ссылку на свежее Android-приложение «Точки»

![Действие копирования ссылки на Android-приложение](./images/versions-report-android-app-link-copy.jpg)

### Ссылка на Android-приложения при принятии приглашения
Ссылка на скачивание Android-приложения на последнем шаге принятия приглашения выездным сотрудником

![Последний шаг при принятии приглашения](./images/invitation-with-android-app-link.jpg)